package com.example.voltappnew.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.voltappnew.R;

import com.example.voltappnew.dbutils.entities.CurrentUser;
import com.example.voltappnew.ui.welcome.WelcomeActivity;

import com.example.voltappnew.dbutils.entities.alldata.VoltData;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;

public class LoginActivity extends AppCompatActivity {
    private EditText username, password;
    private Button login;
    ProgressBar progress_login;

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
   // Connection credentials
    String strUsername;
    String strPassword;
    public static String token ;
    public static String baseUrl = "https://volt-music.herokuapp.com";
    public static String data;
    public static CurrentUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        progress_login = findViewById(R.id.progress_download2);
        progress_login.setVisibility(View.GONE);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Récuperation du nom d'utilisateur et du mot de passe

                //Vérification des identifiants
                strUsername = username.getText().toString();
                strPassword = password.getText().toString();

               // Connexion et redirection
                String loginUrl = baseUrl + "/login";
                VoltData.loginRequest(strUsername, strPassword, loginUrl, data, username, password, token, LoginActivity.this);
                progress_login.setVisibility(View.VISIBLE);

            }
        });

        LoginActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (token != null){
                    progress_login.setVisibility(View.GONE);
                }
            }
        });


    }

    public void initCurrentUser(String str_response) throws JSONException {

        JSONObject json = new JSONObject(str_response);
        currentUser = new CurrentUser();
        currentUser.setIdUtilisateur(json.get("idutilisateur").toString());
        currentUser.setPseudo(json.get("pseudo").toString());
        currentUser.setDateNaissance(json.get("date_naissance").toString());
        currentUser.setEmail(json.get("email").toString());
        currentUser.setNom(json.get("nom").toString());
        currentUser.setPrenom(json.get("prenom").toString());
        currentUser.setNumEtu(json.get("num_etu").toString());
        currentUser.setRole(json.get("role").toString());
    }


}