package com.example.voltappnew.ui.register;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.ui.login.LoginActivity;

public class RegisterActivity extends AppCompatActivity {
    private EditText lastName, firstName, pseudo, email, dateOfBirthday, numEtu, password;
    private Button registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        registerBtn = findViewById(R.id.register_submit);
        lastName = findViewById(R.id.registerLastName);
        firstName = findViewById(R.id.registerFirstName);
        pseudo = findViewById(R.id.registerPseudo);
        email = findViewById(R.id.registerEmail);
        dateOfBirthday = findViewById(R.id.registerDateOfBirthday);
        numEtu = findViewById(R.id.registerNumEtu);
        password = findViewById(R.id.registerPassword);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Inscription puis redirection vers la page de login
                VoltData.registerRequest(lastName.getText().toString(), firstName.getText().toString(), pseudo.getText().toString(), email.getText().toString(), dateOfBirthday.getText().toString(), numEtu.getText().toString(), password.getText().toString(), RegisterActivity.this);
            }
        });
    }
}