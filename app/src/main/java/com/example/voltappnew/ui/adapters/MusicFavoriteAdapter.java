package com.example.voltappnew.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.musique.Musique;

import java.util.ArrayList;
import java.util.Locale;

public class MusicFavoriteAdapter extends RecyclerView.Adapter<MusicFavoriteAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Musique> musicFavoriteArrayList;
    private ArrayList<Musique> arraylist;


    public MusicFavoriteAdapter(Context context, ArrayList<Musique> musicFavoriteArrayList){
        this.context = context;
        this.arraylist = new ArrayList<Musique>();
        if( musicFavoriteArrayList != null ) {
            this.arraylist.addAll(musicFavoriteArrayList);
            this.musicFavoriteArrayList = musicFavoriteArrayList;
        }
        else{
            this.musicFavoriteArrayList = new ArrayList<Musique>();
        }
    }

    @NonNull
    @Override
    public MusicFavoriteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.relative_layout_music_in_playlist, parent, false);
        MusicFavoriteAdapter.ViewHolder viewHolder = new MusicFavoriteAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MusicFavoriteAdapter.ViewHolder holder, int position) {
        holder.musicNametextView.setText(musicFavoriteArrayList.get(position).getTitre());
    }


    @Override
    public int getItemCount() {
        if (musicFavoriteArrayList != null && musicFavoriteArrayList.size() >0){
            return musicFavoriteArrayList.size();
        }else{
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView musicNametextView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            musicNametextView = itemView.findViewById(R.id.music_name2);

        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        musicFavoriteArrayList.clear();
        if (charText.length() == 0) {
            musicFavoriteArrayList.addAll(arraylist);
        } else {
            for (Musique wp : arraylist) {
                if (wp.getTitre().toLowerCase(Locale.getDefault()).contains(charText)) {
                    musicFavoriteArrayList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
