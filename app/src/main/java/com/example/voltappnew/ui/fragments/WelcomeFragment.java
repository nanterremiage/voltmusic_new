package com.example.voltappnew.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.musique.AllMusiques;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.ui.adapters.MusicAdapter;
import com.example.voltappnew.ui.welcome.WelcomeActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class WelcomeFragment extends Fragment {
    private RecyclerView recyclerView, recyclerView1, recyclerView2;
    private ArrayList<AllMusiques> musiqueList, mylist;

    public WelcomeFragment(ArrayList<AllMusiques> musiqueList) {
        this.musiqueList = musiqueList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getContext()).inflate(R.layout.fragment_welcome, container, false);
        //Instanciation de la liste de musique
        recyclerView = view.findViewById(R.id.recycle_view);
        recyclerView1 = view.findViewById(R.id.recycle_view1);
        recyclerView2 = view.findViewById(R.id.recycle_view2);
        this.mylist = new ArrayList<AllMusiques>();
        this.mylist.addAll(WelcomeActivity.musiqueList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView1.setLayoutManager(linearLayoutManager1);
        recyclerView2.setLayoutManager(linearLayoutManager2);

        Collections.shuffle(mylist);
        MusicAdapter musicAdapter = new MusicAdapter(getContext(), this.musiqueList);
        MusicAdapter musicAdapter1 = new MusicAdapter(getContext(), mylist);
        Random random = new Random();
        random.nextInt(10);
        Collections.shuffle(mylist, random);
        MusicAdapter musicAdapter2 = new MusicAdapter(getContext(), mylist);
        recyclerView.setAdapter(musicAdapter);
        recyclerView1.setAdapter(musicAdapter1);
        recyclerView2.setAdapter(musicAdapter2);

        return view;
    }
}
