package com.example.voltappnew.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.album.Album;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.ui.adapters.AlbumAdapter;
import com.example.voltappnew.ui.adapters.MusicAdapter;
import com.example.voltappnew.ui.playeractivity.PlayerActivity;
import com.example.voltappnew.ui.upload.UploadActivity;
import com.example.voltappnew.ui.welcome.WelcomeActivity;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputLayout;

import static com.example.voltappnew.ui.login.LoginActivity.currentUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProfileFragment extends Fragment {
    @Nullable
    public static AlbumAdapter albumAdapter;
    private Button addAlbumBtn;
    private Button addMusicBtn;
    private EditText editTextcomment;
    Activity activity;
    private RecyclerView albumRecyclerView;
    ArrayList <Album> albumList = new ArrayList<Album>();



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getContext()).inflate(R.layout.fragment_profile, container, false);
        activity = (Activity)view.getContext();
        TextView textViewUsername = view.findViewById(R.id.username_id);
        TextView textViewPseudo = view.findViewById(R.id.pseudo_id);

        addAlbumBtn = (Button) view.findViewById(R.id.add_album_btn);

        addAlbumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddAlbumDialog();
            }
        });

        addMusicBtn = view.findViewById(R.id.add_music_btn);
        addMusicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //We create a new intent that would direct us to the player activity
                Intent intent = new Intent(getContext(), UploadActivity.class);
                //Here we start the player activity when we click on one song
                getContext().startActivity(intent);
            }
        });



        TextView textViewDateOfBirthday = view.findViewById(R.id.date_of_birthday_id);
        String year = currentUser.getDateNaissance().substring(0,4);
        String month = currentUser.getDateNaissance().substring(5,7);
        String day = currentUser.getDateNaissance().substring(8,10);
        String strDate = day+" / "+month+" / "+year;

        textViewUsername.setText(currentUser.getNom()+" "+currentUser.getPrenom());
        textViewPseudo.setText("@"+currentUser.getPseudo());
        textViewDateOfBirthday.setText(strDate);

        albumRecyclerView = view.findViewById(R.id.recycle_view_album);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        albumRecyclerView.setLayoutManager(linearLayoutManager);

        albumList = VoltData.albums;

        albumAdapter = new AlbumAdapter(getContext(), albumList);
        albumRecyclerView.setAdapter(albumAdapter);

        return view;
    }




    public void openAddAlbumDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_album, null);

        TextInputLayout textInputLayoutAlbumName = view.findViewById(R.id.add_album_name);

        textInputLayoutAlbumName.getEditText().setText(currentUser.getNom());

        MaterialDatePicker.Builder materialDateBuilder = MaterialDatePicker.Builder.datePicker();
        materialDateBuilder.setTitleText("Sélectionnez votre date de naissance");

        MaterialDatePicker <Long> materialDatePicker = materialDateBuilder.build();

        builder.setView(view).setTitle("Ajouter un nouvel album")
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String titre = textInputLayoutAlbumName.getEditText().getText().toString();
                        VoltData.ajoutAlbum(titre);


                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
