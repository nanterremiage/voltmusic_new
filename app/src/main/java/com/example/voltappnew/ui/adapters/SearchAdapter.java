package com.example.voltappnew.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.musique.AllMusiques;

import java.util.ArrayList;
import java.util.Locale;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private Context context;
    private ArrayList<AllMusiques> musicFavoriteArrayList;
    private ArrayList<AllMusiques> arraylist;


    public SearchAdapter(Context context, ArrayList<AllMusiques> musicFavoriteArrayList){
        this.context = context;
        this.arraylist = new ArrayList<AllMusiques>();
        if( musicFavoriteArrayList != null ) {
            this.arraylist.addAll(musicFavoriteArrayList);
            this.musicFavoriteArrayList = musicFavoriteArrayList;
        }
        else{
            this.musicFavoriteArrayList = new ArrayList<AllMusiques>();
        }
    }

    @NonNull
    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.relative_layout_music_in_playlist, parent, false);
        SearchAdapter.ViewHolder viewHolder = new SearchAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.ViewHolder holder, int position) {
        holder.musicNametextView.setText(musicFavoriteArrayList.get(position).getMusique().getTitre());
        holder.artistNameTextView.setText(musicFavoriteArrayList.get(position).getUtilisateur().getPseudo());
    }


    @Override
    public int getItemCount() {
        if (musicFavoriteArrayList != null && musicFavoriteArrayList.size() >0){
            return musicFavoriteArrayList.size();
        }else{
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView musicNametextView;
        TextView artistNameTextView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            musicNametextView = itemView.findViewById(R.id.music_name2);
            artistNameTextView = itemView.findViewById(R.id.artist_name);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        musicFavoriteArrayList.clear();
        if (charText.length() == 0) {
            musicFavoriteArrayList.addAll(arraylist);
        } else {
            for (AllMusiques wp : arraylist) {
                if (wp.getMusique().getTitre().toLowerCase(Locale.getDefault()).contains(charText)) {
                    musicFavoriteArrayList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
