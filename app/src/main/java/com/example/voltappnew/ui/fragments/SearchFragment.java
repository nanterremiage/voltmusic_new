package com.example.voltappnew.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voltappnew.MainActivity;
import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.dbutils.entities.musique.AllMusiques;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.ui.adapters.MusicAdapter;
import com.example.voltappnew.ui.adapters.MusicFavoriteAdapter;
import com.example.voltappnew.ui.adapters.SearchAdapter;
import com.example.voltappnew.ui.welcome.WelcomeActivity;

import java.util.ArrayList;

public class SearchFragment extends Fragment {
    private RecyclerView recyclerView;
    private SearchView editsearch;
    private View view;
    private ArrayList<AllMusiques> musiques;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getContext()).inflate(R.layout.fragment_search, container, false);
        setHasOptionsMenu(true);
        //Instanciation de la liste de musique
        recyclerView = view.findViewById(R.id.recycle_view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        this.musiques = new ArrayList<AllMusiques>();
        this.musiques.addAll(WelcomeActivity.musiqueList);
        SearchAdapter musicAdapter = new SearchAdapter(getContext(), this.musiques);
        recyclerView.setAdapter(musicAdapter);

        // Locate the EditText in listview_main.xml
        Integer a = R.id.search;
        editsearch = view.findViewById(a);
        editsearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                musicAdapter.filter(text);
                return false;
            }
        });

        return view;
    }


}
