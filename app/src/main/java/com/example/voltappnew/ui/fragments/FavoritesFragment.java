package com.example.voltappnew.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.ui.adapters.MusicFavoriteAdapter;
import com.example.voltappnew.ui.login.LoginActivity;

import java.util.ArrayList;
import java.util.Date;

public class FavoritesFragment extends Fragment {
    private TextView textView;
    private View view;

    private RecyclerView favoriteMusicRecyclerView;
    ArrayList<Musique> favoritesMusicsList = new ArrayList<Musique>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.from(getContext()).inflate(R.layout.fragment_favorites, container, false);
        //Instanciation de la liste des musiques favoris :
        favoriteMusicRecyclerView = view.findViewById(R.id.recycle_view_music_in_favorite);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        favoriteMusicRecyclerView.setLayoutManager(linearLayoutManager);
        //Recupération des musiques favoris :
        VoltData.getFavoritesMusics(LoginActivity.currentUser);
        MusicFavoriteAdapter musicFavoriteAdapter = new MusicFavoriteAdapter(getContext(), VoltData.musiques);
        favoriteMusicRecyclerView.setAdapter(musicFavoriteAdapter);
        return view;
    }


    public void setText(String yourText){
        textView.setText(yourText);
    }
}
