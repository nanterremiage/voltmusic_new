package com.example.voltappnew.ui.welcome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.CurrentUser;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.dbutils.entities.musique.AllMusiques;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.ui.fragments.FavoritesFragment;
import com.example.voltappnew.ui.fragments.LogoutFragment;
import com.example.voltappnew.ui.fragments.ProfileFragment;
import com.example.voltappnew.ui.fragments.SearchFragment;
import com.example.voltappnew.ui.fragments.WelcomeFragment;
import com.example.voltappnew.ui.login.LoginActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class WelcomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private BottomNavigationView bottomNavigationView;
    private TextView favorites;

    //Initialisation des fragments
    LogoutFragment logoutFragment;
     FavoritesFragment fragment1 ;
     SearchFragment fragment2 ;
     public static WelcomeFragment fragment3;
     FragmentManager fm ;
     Fragment active;
     ProfileFragment profileFragment;

    public static String username;
    public static String musiques;


    public static ArrayList<AllMusiques> musiqueList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        // get permission from user
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);
               // return;
            }
        }


        //On récupère l'utilisateur connecté
        String strUsername = getIntent().getStringExtra("Username");
        String token1 = getIntent().getStringExtra("Token");

        username = strUsername;

        Bundle bundle = new Bundle();
        bundle.putString("username", strUsername);
        bundle.putString("musiques",musiques);

        //Initialisation des fragments
        logoutFragment = new LogoutFragment();

        profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);

        fragment1 = new FavoritesFragment();
        fragment1.setArguments(bundle);
        fragment2 = new SearchFragment();
        fragment2.setArguments(bundle);
        fragment3 = new WelcomeFragment(musiqueList);
        fm = getSupportFragmentManager();

        loadFragment(fragment3);

        //Initialisation de la barre de navigation
        bottomNavigationView = findViewById(R.id.bottom_nav_menu);
        bottomNavigationView .setOnNavigationItemSelectedListener(this);
        favorites = findViewById(R.id.favorites_id);
        Toast.makeText(WelcomeActivity.this, strUsername, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 100 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){

        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // get user permission
                //TODO

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);
            }
        }
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.navigation_logout:
                fragment = logoutFragment;
                break;

            case R.id.navigation_favorites:
                fragment = fragment1;
                break;

            case R.id.navigation_search:
                fragment = fragment2;
                break;
            case R.id.navigation_home:
                fragment = fragment3;
                break;
            case R.id.navigation_profile:
                fragment = profileFragment;
                break;
        }
        return loadFragment(fragment);
        }

    public WelcomeFragment getFragment3() {
        return fragment3;
    }

    public static void setFragment3(WelcomeFragment fragment) {
        fragment3 = fragment;
    }

}

