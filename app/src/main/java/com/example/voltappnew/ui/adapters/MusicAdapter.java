package com.example.voltappnew.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.musique.AllMusiques;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.ui.playeractivity.PlayerActivity;

import java.util.ArrayList;
import java.util.Locale;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.ViewHolder> {
    private Context context;
    private ArrayList<AllMusiques> musiques = new ArrayList<AllMusiques>();
    public static ArrayList<Integer> images;

    public MusicAdapter(Context context, ArrayList<AllMusiques> musiques){
        this.context = context;
        this.musiques = musiques;
        initImage();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.relative_layout_music_in_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(musiques.get(position).getMusique().getTitre());
        holder.artistNameTextView.setText(musiques.get(position).getUtilisateur().getPseudo());
        //holder.textView.setText(musiques.get(position).get);
        holder.imageView.setBackgroundResource(images.get(position%8));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //We create a new intent that would direct us to the player activity
                Intent intent= new Intent(context, PlayerActivity.class);
                intent.putExtra("idmusique", musiques.get(position).getMusique().getIdMusique());
                intent.putExtra("titre", musiques.get(position).getMusique().getTitre());
                

                //Here we start the player activity when we click on one song
                context.startActivity(intent);
            }
        });

        holder.musicMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // à voir comment l'utiliser pour la selection : https://developer.android.com/guide/topics/ui/menus.html#xml
                PopupMenu popup = new PopupMenu(context, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.gerer_music_menu, popup.getMenu());
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (musiques != null && musiques.size() >0){
            return musiques.size();
        }else{
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        ImageView imageView;
        TextView durationPlayedTextView;
        TextView artistNameTextView;
        ImageView musicMenuImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.music_name1);
            imageView = itemView.findViewById(R.id.music_img1);
            durationPlayedTextView = itemView.findViewById(R.id.duration_played);
            artistNameTextView = itemView.findViewById(R.id.artist_name1);
            musicMenuImageView = itemView.findViewById(R.id.music_menu_btn1);
        }
    }




    public void initImage(){
        images = new ArrayList<>();
        images.add(R.drawable.im1);
        images.add(R.drawable.im2);
        images.add(R.drawable.im3);
        images.add(R.drawable.im5);
        images.add(R.drawable.im6);
        images.add(R.drawable.im7);
        images.add(R.drawable.im8);
        images.add(R.drawable.im9);
        images.add(R.drawable.im10);


    }


}
