package com.example.voltappnew.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.album.Album;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.ui.playeractivity.PlayerActivity;

import java.util.ArrayList;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Album> albums;

    public AlbumAdapter(Context context, ArrayList<Album> albums){
        this.context = context;
        this.albums = albums;
    }

    @NonNull
    @Override
    public AlbumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.relative_layout_album, parent, false);
        AlbumAdapter.ViewHolder viewHolder = new AlbumAdapter.ViewHolder(view);
        return viewHolder;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.ViewHolder holder, int position) {
        holder.albumNametextView.setText(albums.get(position).getTitre());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.albumMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(context, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.gerer_album_menu, popup.getMenu());
                popup.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        if (albums != null && albums.size() >0){
            return albums.size();
        }else{
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView albumNametextView;
        ImageView albumMenuImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            albumNametextView = itemView.findViewById(R.id.album_name);
            albumMenuImageView = itemView.findViewById(R.id.album_menu);
        }
    }

}
