package com.example.voltappnew.ui.upload;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.album.Album;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.ui.playeractivity.PlayerActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UploadActivity extends AppCompatActivity {
    Button chooseFileBtn, upload_file_submit_btn;
    String path_upload;
    Uri uri_upload;
    Spinner spinner;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        chooseFileBtn = findViewById(R.id.choose_file_btn);
        upload_file_submit_btn = findViewById(R.id.upload_file_submit_btn);
        spinner = findViewById(R.id.spinner1);
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray = VoltData.albums.stream().map(Album::getTitre).collect(Collectors.toList());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected_item = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), "Ajouter la musique à l'album : " + selected_item,          Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
            }
        });

        chooseFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_upload = new Intent();
                intent_upload.setType("audio/*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload,1);
            }
        });

        upload_file_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoltData.uploadMusiqueRequest
                        (path_upload, uri_upload);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){

        if(requestCode == 1){

            if(resultCode == RESULT_OK){
                System.out.println("Mon datatatata --->"+data);
                //the selected audio.
                uri_upload = data.getData();
                path_upload = new String();
                path_upload =  getAudioPath(uri_upload);


            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String getAudioPath(Uri uri) {
        //System.out.println(""uri.getPath());

        String[] data = {MediaStore.Audio.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


}