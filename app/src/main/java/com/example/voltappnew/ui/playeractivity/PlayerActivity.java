package com.example.voltappnew.ui.playeractivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.alldata.VoltData;
import com.example.voltappnew.ui.login.LoginActivity;
import com.example.voltappnew.ui.welcome.WelcomeActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;

public class PlayerActivity extends AppCompatActivity {
    public static File folder, file;
    ImageView img_player, prev, suivant;
    TextView song_name, duration_played, duration_total, path;
    SeekBar seekbar_value;
    FloatingActionButton play_pause;
    ProgressBar progress_download;
    int position = -1;
    public static String path_upload;
    String titre;
    static Uri uri, uri_upload;
    static MediaPlayer mediaPlayer;
    private Handler handler = new Handler(Looper.myLooper());
    private Thread playThread, previousThread, nextThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        InitialiseViews();
        // get the id of the music to download
        position = getIntent().getIntExtra("idmusique", 1);
        titre = getIntent().getStringExtra("titre");

        //Download musique
        VoltData.downloadMusiqueRequest(position, LoginActivity.token, PlayerActivity.this,
                 mediaPlayer, play_pause);


        PlayerActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    progress_download.setVisibility(View.GONE);
                    play_pause.setEnabled(true);
                    int current_position = mediaPlayer.getCurrentPosition() / 1000;
                    seekbar_value.setProgress(current_position);
                    duration_played.setText(Timeformat(current_position));
                }
                handler.postDelayed(this, 1000);
            }
        });


    }



    public void InitialiseViews() {
        img_player = findViewById(R.id.img_player);
        prev = findViewById(R.id.prev);
        suivant = findViewById(R.id.suivant);
        song_name = findViewById(R.id.song_name);
        duration_played = findViewById(R.id.duration_played);
        duration_total = findViewById(R.id.duration_total);
        seekbar_value = findViewById(R.id.seekbar_value);
        play_pause = findViewById(R.id.play_pause);
        play_pause.setEnabled(false);
        progress_download = findViewById(R.id.progress_download);
        progress_download.setVisibility(View.VISIBLE);

    }
    private String Timeformat(int current_position) {
        String seconds = String.valueOf(current_position % 60);
        String minutes = String.valueOf(current_position / 60);
        if (seconds.length() == 1) {
            return minutes + ":" + "0" + seconds;
        } else return minutes + ":" + seconds;
    }


    public void musicInfo() throws IOException {
        uri = Uri.parse(file.getPath());
        create_mediapplayer();
        //Initialisation de la durée total de la musique
        int total_duration = mediaPlayer.getDuration()/1000;
        duration_total.setText(Timeformat(total_duration));
        song_name.setText(titre);


        seekbar_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mediaPlayer != null && b) {
                    mediaPlayer.seekTo(i * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


    @Override
    protected void onPostResume() {
        playThreadBtn();
        nextThreadBtn();
        previousThreadBtn();
        super.onPostResume();
    }

    private void playThreadBtn() {
        playThread = new Thread(){
            @Override
            public void run() {
                super.run();
                play_pause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        playPausedClicked();
                    }
                });
            }
        };
        playThread.start();
    }
    private void playPausedClicked() {
        if(mediaPlayer.isPlaying()){
            play_pause.setImageResource(R.drawable.ic__play_circle);
            mediaPlayer.pause();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
        }else{
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            mediaPlayer.start();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
        }
    }

    private void previousThreadBtn() {
        previousThread = new Thread(){
            @Override
            public void run() {
                super.run();
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prevClicked();
                    }
                });
            }
        };
        previousThread.start();
    }

    private void prevClicked() {
        if (mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            mediaPlayer.start();
        }else{
            mediaPlayer.stop();
            mediaPlayer.release();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic__play_circle);
        }
    }
    private void nextThreadBtn() {
        nextThread = new Thread(){
            @Override
            public void run() {
                super.run();
                suivant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        nextClicked();
                    }
                });
            }
        };
        nextThread.start();
    }
    private void nextClicked() {
        if (mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            mediaPlayer.start();
        }else{
            mediaPlayer.stop();
            mediaPlayer.release();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic__play_circle);
        }
    }

    public void create_file(int id) throws IOException {
        folder = new File(PlayerActivity.this.getExternalFilesDir(null), "/Downloads");
        if (!folder.exists()) {
            boolean folderCreated = folder.mkdir();
            Log.v("folderCreated", folderCreated + "");
        }
        file = new File(folder.getPath() + "/downloadedAudio"+id+".mp3");
        if (!file.exists()) {
            boolean fileCreated = file.createNewFile();
            Log.v("fileCreated", fileCreated + "");
            System.out.println(file.getAbsolutePath());
        }
    }



    public void create_mediapplayer() throws IOException {

        mediaPlayer = new MediaPlayer();
        mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
        System.out.println("Media  " +mediaPlayer);


    }

    public void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}