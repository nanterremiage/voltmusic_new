package com.example.voltappnew.dbutils.entities.utilisateurs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    @JsonProperty("_embedded")
    private com.example.voltappnew.dbutils.entities.utilisateurs.Embedded embedded;

    public Response() {
    }

    public com.example.voltappnew.dbutils.entities.utilisateurs.Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }
}
