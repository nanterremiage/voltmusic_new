package com.example.voltappnew.dbutils;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.voltappnew.dbutils.dao.UserDao;
import com.example.voltappnew.dbutils.entities.User;

//Ici nous avons notre base de données
@Database(entities = {User.class} , version = 1)
public abstract class LocalVoltDB extends RoomDatabase {
    public abstract UserDao userDao();
}
