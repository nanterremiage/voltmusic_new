package com.example.voltappnew.dbutils.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//Cette classe defini le modèle utilisateur
@Entity
public class User {
    @PrimaryKey
    @NonNull
    private String userToken;

    @ColumnInfo(name = "pseudo")
    private String pseudo;

    @ColumnInfo(name = "prenom")
    private String prenom;

    @ColumnInfo(name = "nom")
    private String nom;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "date_naissance")
    private String dateNaissance;

    @ColumnInfo(name = "num_etu")
    private Integer numEtu;

    @NonNull
    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(@NonNull String userToken) {
        this.userToken = userToken;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Integer getNumEtu() {
        return numEtu;
    }

    public void setNumEtu(Integer numEtu) {
        this.numEtu = numEtu;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @ColumnInfo(name = "role")
    private String role;
}
