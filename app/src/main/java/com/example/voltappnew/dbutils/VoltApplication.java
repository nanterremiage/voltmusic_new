package com.example.voltappnew.dbutils;

import android.app.Application;

import androidx.room.Room;
//Dans cette classe on crée une application qui permettra d'avoir une seule instance de notre base de données
public class VoltApplication extends Application {
    private static VoltApplication voltApplicationinstance;
    private LocalVoltDB dbInstance;



    @Override
    public void onCreate() {
        super.onCreate();

        voltApplicationinstance = this;
        dbInstance = Room.databaseBuilder(getApplicationContext(), LocalVoltDB.class, "LocalDB").build();
    }

    public VoltApplication getVoltApplicationinstance(){
        return voltApplicationinstance;
    }
    public LocalVoltDB getDbInstance() {
        return dbInstance;
    }
}
