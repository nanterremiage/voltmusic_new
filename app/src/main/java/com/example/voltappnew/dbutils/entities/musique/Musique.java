package com.example.voltappnew.dbutils.entities.musique;

import com.example.voltappnew.dbutils.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Musique {
    private int idMusique;
    private String titre;
    private String path;
    private Date dateSortie;

    public Musique() {
    }

    public Musique(int idMusique, String titre, String path, Date dateSortie) {
        this.idMusique = idMusique;
        this.titre = titre;
        this.path = path;
        this.dateSortie = dateSortie;
    }

    public int getIdMusique() {
        return idMusique;
    }

    public void setIdMusique(int idMusique) {
        this.idMusique = idMusique;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }


}
