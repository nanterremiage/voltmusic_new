package com.example.voltappnew.dbutils.entities.musique;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseAllMusiques {
    @JsonProperty("musiques")
    ArrayList<AllMusiques> allMusiques  = new ArrayList<AllMusiques>();

    public ResponseAllMusiques() {
    }

    public ArrayList<AllMusiques> getAllMusiques() {
        return allMusiques;
    }

    public void setAllMusiques(ArrayList<AllMusiques> allMusiques) {
        this.allMusiques = allMusiques;
    }
}
