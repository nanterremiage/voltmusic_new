package com.example.voltappnew.dbutils.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.voltappnew.dbutils.entities.User;

import static androidx.room.OnConflictStrategy.REPLACE;
//Cette interface defini les differentes opérations qu'on peut mener sur un utilisateur
@Dao
public interface UserDao {
    @Query("SELECT * FROM user WHERE pseudo like :strUsername")
    User getUserbyUsername(String strUsername);

    @Query("SELECT * FROM user LIMIT 1;")
    User getUser();

    @Query("SELECT * FROM user WHERE usertoken = (:userId)")
    User loadById(int userId);

    @Insert(onConflict = REPLACE)
    void insertUser(User user);

    @Update
    void updateUser(User user);

    @Query("DELETE FROM user;")
    void deleteAllUsers();
}
