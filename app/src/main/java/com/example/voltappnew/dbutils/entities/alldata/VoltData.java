package com.example.voltappnew.dbutils.entities.alldata;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.voltappnew.R;
import com.example.voltappnew.dbutils.entities.CurrentUser;
import com.example.voltappnew.dbutils.entities.album.Album;
import com.example.voltappnew.dbutils.entities.musique.Musique;
import com.example.voltappnew.dbutils.entities.musique.AllMusiques;
import com.example.voltappnew.dbutils.entities.musique.ResponseAllMusiques;
import com.example.voltappnew.dbutils.entities.utilisateurs.Utilisateur;
import com.example.voltappnew.ui.fragments.ProfileFragment;
import com.example.voltappnew.ui.fragments.WelcomeFragment;
import com.example.voltappnew.ui.login.LoginActivity;
import com.example.voltappnew.ui.playeractivity.PlayerActivity;
import com.example.voltappnew.ui.register.RegisterActivity;
import com.example.voltappnew.ui.upload.UploadActivity;
import com.example.voltappnew.ui.utils.Validation;
import com.example.voltappnew.ui.welcome.WelcomeActivity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;

import static com.example.voltappnew.ui.login.LoginActivity.JSON;
import static com.example.voltappnew.ui.login.LoginActivity.currentUser;
import static com.example.voltappnew.ui.welcome.WelcomeActivity.musiqueList;

public class VoltData {
    public static ArrayList<Musique> musiques;
    public static ArrayList<AllMusiques> allMusiques;
    public static  ArrayList<Album> albums;
    public static ArrayList<Musique> favoritesMusicsList;
    public static ArrayList<Utilisateur> utilisateurs;
    public static String mytoken;
    public static String object = new String();

    public static void loginRequest(String strUsername, String strPassword, String url, String data
            , EditText username, EditText password, String token, LoginActivity loginActivity){

        OkHttpClient client = new OkHttpClient();

        JSONObject jo = new JSONObject();
        try {
            jo.put("email", strUsername);
            jo.put("password",strPassword);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(jo.toString(),JSON);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String test = response.body().string();
                loginActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            LoginActivity.token = response.header("Authorization");
                            mytoken = LoginActivity.token;
                            if ( LoginActivity.token!= null) {
                                loginActivity.initCurrentUser(test);

                                JSONObject json = new JSONObject(test);
                                LoginActivity.data = json.get("nom").toString() + json.get("prenom").toString();
                                Intent intent = new Intent(loginActivity, WelcomeActivity.class);
                                intent.putExtra("Username", data);
                                intent.putExtra("Token", LoginActivity.token);
                                getAlbums();
                                getMusiquesRequest(LoginActivity.token);
                                //while (allMusiques == null || albums ==null)
                                try {
                                    synchronized(object) {
                                        while(albums == null || musiqueList == null) {
                                            object.wait();
                                            object.wait();
                                        }
                                        loginActivity.startActivity(intent);
                                    }
                                } catch (InterruptedException e) {
                                    System.out.println("Erreur d'attente" + e);
                                }
                            }else if (LoginActivity.token == null){
                                username.setText(" ");
                                password.setText(" ");
                                Toast.makeText(loginActivity, "Identifiant ou mot de passe incorrect", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });



            }
        });

    }

    public static void registerRequest(String lastName, String firstName, String pseudo, String email, String dateOfBirthday, String numEtu, String password, RegisterActivity registerActivity){
        try {
            boolean existsError = false;
            if(pseudo.isEmpty()) {
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerPseudo)).setError("Champ obligatoire.");
            }
            if(firstName.isEmpty()){
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerFirstName)).setError("Champ obligatoire.");
            }
            if(lastName.isEmpty()){
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerLastName)).setError("Champ obligatoire.");
            }
            if(dateOfBirthday.isEmpty()){
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerDateOfBirthday)).setError("Champ obligatoire.");
            }
            if(!Validation.isValidNumber(numEtu)){
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerNumEtu)).setError("Numéro étudiant invalid");
            }
            if(!Validation.isValidEmail(email)){
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerEmail)).setError("E-mail invalide");
            }
            if(!Validation.isValidPassword(password))
            {
                existsError = true;
                ((EditText) registerActivity.findViewById(R.id.registerPassword)).setError("Le mot de passe doit avoir plus de 5 caractères");
            }
            if(!existsError) {
                OkHttpClient client = new OkHttpClient().newBuilder().build();
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                //dateTabe va nous permettre de récupérer la date au format : JJ-MM-ANNEE
                String[] dateTab = dateOfBirthday.split("/");
                //Création de l'objet JSON.
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("nom", lastName)
                        .put("prenom", firstName)
                        .put("pseudo", pseudo)
                        .put("email", email)
                        .put("dateNaissance", dateTab[2] + "-" + dateTab[1] + "-" + dateTab[0])
                        .put("numEtu", numEtu)
                        .put("role", "USER")
                        .put("password", password);
                RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                Request request = new Request.Builder()
                        .url(LoginActivity.baseUrl+"/utilisateur/signup")
                        .method("POST", body)
                        .addHeader("Content-Type", "application/json")
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        Toast.makeText(registerActivity, "La tentative d'inscription a échouée.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        //Bien inscrit !
                        Intent intent = new Intent(registerActivity, LoginActivity.class);
                        registerActivity.startActivity(intent);
                        Toast.makeText(registerActivity, "Votre compte a été créé avec succès.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }catch (final Throwable th) {
            System.out.println(th.getMessage());
        }
    }


    public static void getUtilisateurs(){
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(LoginActivity.baseUrl+"/utilisateurs")
                .header("Authorization", LoginActivity.token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String test = response.body().string();
                com.example.voltappnew.dbutils.entities.utilisateurs.Response resp;
                ObjectMapper objectMapper = new ObjectMapper();
                resp = objectMapper.readValue(test, com.example.voltappnew.dbutils.entities.utilisateurs.Response.class);
                utilisateurs = resp.getEmbedded().getUtilisateurs();
            }
        });
    }

    public static void getAlbums(){
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(LoginActivity.baseUrl+"/albums")
                .header("Authorization", LoginActivity.token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String test = response.body().string();
                com.example.voltappnew.dbutils.entities.album.Response resp;
                ObjectMapper objectMapper = new ObjectMapper();
                resp = objectMapper.readValue(test, com.example.voltappnew.dbutils.entities.album.Response.class);
                synchronized(object) {
                    albums = resp.getEmbedded().getAlbums();
                    object.notify();
                }
                if (ProfileFragment.albumAdapter != null) {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            ProfileFragment.albumAdapter.setAlbums(albums);
                            ProfileFragment.albumAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    public static void getFavoritesMusics(CurrentUser currentUser_1){
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(LoginActivity.baseUrl +"/utilisateurs/"+currentUser_1.getIdUtilisateur()+"/aimeMusiques")
                .header("Authorization", LoginActivity.token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String test = response.body().string();
                com.example.voltappnew.dbutils.entities.musique.Response resp;
                ObjectMapper objectMapper = new ObjectMapper();
                resp = objectMapper.readValue(test, com.example.voltappnew.dbutils.entities.musique.Response.class);
                favoritesMusicsList = resp.getEmbedded().getMusiques();
            }
        });
    }


    public static void getMusiquesRequest(String token){
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(LoginActivity.baseUrl +"/all/musiques")
                .header("Authorization", token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String test = response.body().string();
                ResponseAllMusiques resp;
                ObjectMapper objectMapper = new ObjectMapper();
                resp = objectMapper.readValue(test, ResponseAllMusiques.class);
                allMusiques = resp.getAllMusiques();
                //TODO Supprimer ceci une fois l'adaptation complète
                synchronized(object) {
                    musiqueList = allMusiques;
                    WelcomeActivity.setFragment3(new WelcomeFragment(musiqueList));
                    object.notify();
                }


            }
        });


    }

    public static void downloadMusiqueRequest(int id, String token, PlayerActivity playerActivity
            , MediaPlayer mediaPlayer, FloatingActionButton play_pause){
        playerActivity.stopPlaying();
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(LoginActivity.baseUrl+"/file/download/").newBuilder();
        urlBuilder.addQueryParameter("idmusique", String.valueOf(id));

        Request request = new Request.Builder()
                .url(String.valueOf(urlBuilder))
                .method("GET", null)
                .addHeader("Authorization", token)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            //Création d'un fichier
                            playerActivity.create_file(id);
                            BufferedSink sink = Okio.buffer(Okio.sink(PlayerActivity.file));
                            sink.writeAll(response.body().source());
                            // Toast.makeText(PlayerActivity.this, "La musique", Toast.LENGTH_SHORT).show();
                            sink.close();
                            playerActivity.musicInfo();
                            if (mediaPlayer != null) {
                                Toast.makeText(playerActivity, "La musique est téléchagé", Toast.LENGTH_SHORT).show();
                                mediaPlayer.start();
                            } else {
                                playerActivity.create_mediapplayer();
                                mediaPlayer.start();
                                play_pause.setImageResource(R.drawable.ic__play_circle);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();

            }
        });


    }

    public static void uploadMusiqueRequest(String path_upload, Uri myuri){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        File myfile = new File(String.valueOf(path_upload));
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("idalbum","1")
                .addFormDataPart("file", path_upload,
                        RequestBody.create(myfile, MediaType.parse("application/octet-stream")))
                .addFormDataPart("titre", "Volta")
                .build();
        Request request = new Request.Builder()
                .url(LoginActivity.baseUrl+"/file/upload")
                .method("POST", body)
                .addHeader("Authorization", LoginActivity.token)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                System.out.println("Echec Upload");
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        response.body().close();
                    }
                });
            }
        });
    }

    public static void ajoutAlbum(String titre){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("titre",titre)
                .addFormDataPart("description", "Test")
                .build();
        Request request = new Request.Builder()
                .url(LoginActivity.baseUrl+"/ajout/album")
                .header("Authorization", LoginActivity.token)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                getAlbums();
            }
        });


    }
}
