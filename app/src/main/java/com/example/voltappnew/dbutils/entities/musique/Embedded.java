package com.example.voltappnew.dbutils.entities.musique;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Embedded {
    @JsonProperty("musiques")
    ArrayList<Musique> musiques = new ArrayList<Musique>();

    public Embedded(){

    }

    public ArrayList<Musique> getMusiques() {
        return musiques;
    }

    public void setMusiques(ArrayList<Musique> musique) {
        this.musiques = musique;
    }
}
