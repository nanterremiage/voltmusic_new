package com.example.voltappnew.dbutils.entities.utilisateurs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Embedded {
    @JsonProperty("utilisateurs")
    ArrayList<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

    public Embedded(){

    }

    public ArrayList<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(ArrayList<Utilisateur> utilsateurs) {
        this.utilisateurs = utilisateurs;
    }
}
