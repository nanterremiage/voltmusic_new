package com.example.voltappnew.dbutils.entities.album;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Embedded {
    @JsonProperty("albums")
    ArrayList<Album> albums = new ArrayList<Album>();

    public Embedded(){

    }
    public ArrayList<Album> getAlbums() {
        return albums;
    }
    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

}
