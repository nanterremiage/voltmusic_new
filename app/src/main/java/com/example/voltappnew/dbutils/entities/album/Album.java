package com.example.voltappnew.dbutils.entities.album;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {
    private int idalbum;
    private String titre;
    private String description;
    private Date dateSortie;

    public Album() {
    }

    public Album(int idalbum, String titre, String description, Date dateSortie) {
        this.idalbum = idalbum;
        this.titre = titre;
        this.description = description;
        this.dateSortie = dateSortie;
    }

    public int getIdalbum() {
        return idalbum;
    }

    public void setIdalbum(int idalbum) {
        this.idalbum = idalbum;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }
}
