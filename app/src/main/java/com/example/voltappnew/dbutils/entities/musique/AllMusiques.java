package com.example.voltappnew.dbutils.entities.musique;

import com.example.voltappnew.dbutils.entities.User;
import com.example.voltappnew.dbutils.entities.album.Album;
import com.example.voltappnew.dbutils.entities.utilisateurs.Utilisateur;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllMusiques {

    private Musique musique;
    private Utilisateur utilisateur;
    private Album album;

    public AllMusiques() {

    }

    public Musique getMusique() {
        return musique;
    }

    public void setMusique(Musique musique) {
        this.musique = musique;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }
}
