# Introduction

Ce projet a été développé par 4 étudiants de Nanterre. Il s’agit d’une application Android de partage de musique. Pour plus d’information rendez-vous sur notre [site web](https://volt-music.herokuapp.com/home).

# Comment installer l'application ?

Rien de plus simple :

- D’abord, il faut cloner le projet. 
- Ensuite, ouvrir un IDE Android, vous pouvez opter pour [Android Studio]( https://developer.android.com/studio)
- Enfin, une fois votre IDE ouverte, vous pouvez lancer l’application soit sur un émulateur de votre choix, soit sur un smartphone Android. Découvrez comment le faire en suivant [la documentation officielle](https://developer.android.com/studio/run/emulator). 
